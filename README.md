I. Mise en place de l'environnement :
Nous avons créé un projet GitLab pour le TP et un dépôt Git local pour notre groupe. Nous avons ensuite cloné le dépôt Git local sur notre machine pour commencer le développement de l'application.

II. Développement de l'application :
Nous avons créé une application AdonisJS simple et vierge. Nous avons ensuite ajouté le code source au dépôt Git local et vérifié que l'application pouvait être lancée en local.

III. Configuration de Docker :
Nous avons créé un fichier Dockerfile pour containeriser l’application. voir .dockerfile

IV. Configuration de GitLab CI/CD :
Nous avons créé un fichier .gitlab-ci.yml Nous avons intégré un test simple pour vérifier que l'application peut être construite avec succès.Le pipeline comporte une seule étape (stage) appelée "build". Dans cette étape, nous utilisons l'image Docker officielle "docker:stable" pour exécuter les commandes de construction de notre image.

V. Test et déploiement :
Nous avons poussé les modifications sur GitLab et vérifié l'exécution du pipeline CI/CD sur GitLab.

VI. Collaboration :
Nous avons ajouté une fonctionnalité simple à l’application, puis nous avons utilisé les fonctionnalités de merge requests de GitLab pour intégrer les modifications de chaque membre du groupe.
