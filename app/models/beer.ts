import { BaseModel, column } from '@adonisjs/lucid/orm'


export default class Beer extends BaseModel {
  @column({ isPrimary: true })
  public id: number = 0 // Initialise avec une valeur par défaut

  @column()
  public name: string = ''

  @column()
  public type: string = ''
}