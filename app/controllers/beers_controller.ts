 import type { HttpContext } from '@adonisjs/core/http'
import Beer from '#models/beer'


export default class beers_controller {
    public async index({}: HttpContext) {
        return Beer.all()
      }
    
      public async show({ params }: HttpContext) {
        return Beer.findOrFail(params.id)
      }
    
      public async store({ request }: HttpContext) {
        const data = request.only(['name', 'type', 'brewery_id'])
        return Beer.create(data)
      }
    
      public async update({ params, request }: HttpContext) {
        const beer = await Beer.findOrFail(params.id)
        const data = request.only(['name', 'type', 'brewery_id'])
        beer.merge(data)
        await beer.save()
        return beer
      }
    
      public async destroy({ params }: HttpContext) {
        const beer = await Beer.findOrFail(params.id)
        await beer.delete()
        return { message: 'Beer deleted' }
      }
    }