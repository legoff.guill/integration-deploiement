/*
|--------------------------------------------------------------------------
| Routes file
|--------------------------------------------------------------------------
|
| The routes file is used for defining the HTTP routes.
|
*/

import router from '@adonisjs/core/services/router'
import beers_controller from '#controllers/beers_controller'

router.get('/', async () => {
  return {
    hello: 'world',
  }
})


router.get('beers', [beers_controller, 'index'])
router.get('beers/:id', [beers_controller, 'show'])
router.post('beers', [beers_controller, 'store'])
router.put('beers/:id', [beers_controller, 'update'])
router.delete('beers/:id', [beers_controller, 'destroy'])


